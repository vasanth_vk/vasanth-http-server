const fs = require("fs");
const path = require("path");

const readFile = (filepath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, filepath), "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = readFile;
