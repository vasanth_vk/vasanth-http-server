const http = require("http");
const generatev4 = require("./generatev4");
const readFile = require("./readFile");
const PORT = 8000;

const server = http.createServer((req, res) => {
    switch (true) {
        case req.url === "/": {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.write("HOME");
            res.end();
            break;
        }

        case req.url === "/html": {
            readFile("./index.html")
                .then((data) => {
                    res.writeHead(200, { "Content-Type": "text/html" });
                    res.write(data);
                    res.end();
                })
                .catch((err) => {
                    res.writeHead(500, { "Content-Type": "text/html" });
                    console.log(err);
                    res.write(`${req.url} 500:not found`);
                    res.end();
                });
            break;
        }

        case req.url === "/json": {
            readFile("./myJson.json")
                .then((data) => {
                    res.writeHead(200, { "Content-Type": "application/json" });
                    res.write(JSON.stringify(JSON.parse(data), null, 4));
                    res.end();
                })
                .catch((err) => {
                    res.writeHead(500, { "Content-Type": "text/html" });
                    console.log(err);
                    res.write(`${req.url} 500:not found`);
                    res.end();
                });
            break;
        }

        case req.url === "/uuid": {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.write(generatev4());
            res.end();
            break;
        }

        case req.url.includes(`/status/`): {
            let statusCode = req.url.split("/");
            if (
                statusCode.length == 3 &&
                statusCode[2] <= 599 &&
                statusCode[2] >= 100
            ) {
                res.writeHead(statusCode[2], { "Content-Type": "text/plain" });
                res.write(req.url);
                res.end();
                break;
            } else {
                res.writeHead(404, { "Content-Type": "text/plain" });
                res.write(`${req.url} 404:not found`);
                res.end();
                break;
            }
        }

        case req.url.includes(`/delay/`): {
            let delayArray = req.url.split("/");
            if (delayArray.length == 3 && !isNaN(delayArray[2])) {
                let delay = parseFloat(delayArray[2]);
                setTimeout(() => {
                    res.writeHead(200, { "Content-Type": "text/plain" });
                    res.write(req.url);
                    res.end();
                }, delay * 1000);
                break;
            } else {
                res.writeHead(404, { "Content-Type": "text/plain" });
                res.write(`${req.url} 404:not found`);
                res.end();
                break;
            }
        }

        default: {
            res.writeHead(404, { "Content-Type": "text/plain" });
            res.write(`${req.url} 404:not found`);
            res.end();
        }
    }
});

server.listen(PORT, () => {
    console.log(`Server running at port ${PORT}`);
});
