const { v4 } = require("uuid");

const generatev4 = () => {
    return JSON.stringify(
        {
            uuid: v4(),
        },
        null,
        4
    );
};

module.exports = generatev4;
